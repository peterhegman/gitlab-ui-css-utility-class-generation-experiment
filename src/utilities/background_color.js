const { includeImportantVariant } = require('../utils');

module.exports = includeImportantVariant(['gl-bg-transparent', 'gl-bg-white']);
