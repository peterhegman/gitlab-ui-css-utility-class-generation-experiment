const includeImportantVariant = (cssClasses) =>
  cssClasses.flatMap((cssClass) => [cssClass, `${cssClass}!`]);

module.exports = { includeImportantVariant };
