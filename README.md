## Setup

- Utilities are generated by [Tailwind](https://tailwindcss.com/docs) based on [`tailwind.config.js`](https://gitlab.com/peterhegman/gitlab-ui-css-utility-class-generation-experiment/-/blob/master/tailwind.config.js).
- The [PurgeCSS safelist](https://purgecss.com/safelisting.html) is configured in [`tailwind.config.js`](https://gitlab.com/peterhegman/gitlab-ui-css-utility-class-generation-experiment/-/blob/master/tailwind.config.js#L10) and imports the "Safe" classes from [`src/utilities/*.js`](https://gitlab.com/peterhegman/gitlab-ui-css-utility-class-generation-experiment/-/tree/master/src/utilities).
- [`includeImportantVariant`](https://gitlab.com/peterhegman/gitlab-ui-css-utility-class-generation-experiment/-/blob/master/src/utils.js#L1) utility function is used to add the `!important` variant automatically.
- All the CSS classes are generated and then everything except what is defined in the [safelist](https://gitlab.com/peterhegman/gitlab-ui-css-utility-class-generation-experiment/-/blob/master/tailwind.config.js#L10) is stripped out.

## Adding a new CSS utility class

- Locate the correct category file in [`src/utilities`](https://gitlab.com/peterhegman/gitlab-ui-css-utility-class-generation-experiment/-/tree/master/src/utilities).
- Add the CSS class to the exported array.

## Adding a new CSS category

- Create a new file in [`src/utilities`](https://gitlab.com/peterhegman/gitlab-ui-css-utility-class-generation-experiment/-/tree/master/src/utilities). e.g. `flex.js`.
- Export an array with the CSS classes to include.
- Import the new file in [`tailwind.config.js`](https://gitlab.com/peterhegman/gitlab-ui-css-utility-class-generation-experiment/-/blob/master/tailwind.config.js) and add to [`purge.options.safelist`](https://gitlab.com/peterhegman/gitlab-ui-css-utility-class-generation-experiment/-/blob/master/tailwind.config.js#L10).

## Build CSS

`yarn build`
